//name = Caitlin
//Date = 29/03/18
//id = 30002731
//Purpose = to write and if / else statement to work out the cost of electricity depending on the units used
//define variables used
let id = 0
let name = ""
let units = 0
let total = 0
let price = 0
//all three of thes prompt the user to input a number or a word(for the second one)
id = Number(prompt("Please enter the customer's ID number."))
name = prompt("Please enter the customers name.")
units = Number(prompt("Please enter the number of units that " + name + " has used."))
//else/if statements - works out the price of the units depending on the users inputs
if (units < 200){
    price = 1.20
    total = price * units
}else if(units > 200 && units < 400){
    price = 1.50
    total = price * units
}else if(units > 400 && units < 600){
    price = 1.80
    total = price * units
}else{
    price = 2.00
    total = price * units
}
//will be displayed with all the user's input information on the html
console.log(`This application will calculate the customer's Electricity Bill:`)
console.log(`----------------------------------------------------------------`)
console.log(`ID:                                        ${id}`)
console.log(`Name:                                      ${name}`)
console.log(`Units:                                     ${units}`)
console.log(``)
console.log(`Electricity Bill`)
console.log(`----------------------------------------------------------------`)
console.log(`The total owing @ $${price.toFixed(2)} per unit is:    $${total.toFixed(2)}`)