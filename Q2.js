//name = Caitlin
//Date = 29/03/18
//id = 30002731
//Purpose = To add all the 10 numbers in the loop and find the average of those numbers

//declare variables used
let start = 1
let end = 10
let str1 = 0
let average = 0
//will display this as the heading on the html
console.log(`This application will find the average of ten numbers:`)
console.log(`------------------------------------------------------`)
//my for loop
for(i = start; i <= end; i++){
    str1 = Number(prompt(`Please enter number ${i}`))//prompts user to input number
    console.log(`Number ${i} : ${str1}`) //shows each of the user's inputs
    average = average + str1 / 10   //calculation to find the average
}
 console.log(`The average of those 10 numbers is ${average}`)//displays this at the end to show the average