//name = Caitlin
//Date = 29/03/18
//id = 30002731
//Purpose = Use a switch statement to display if the user's input is a vowel of a consonant
//define variable used
let letter
let type
//prompts the user to input a letter from the alphabet
letter = prompt("Enter a letter of the alphabet to determine if it is a vowel or a consonant")
//my switch - makes the program say vowel if it is one and anything else would be a consonant
switch(letter){
    case "a":
    case "e":
    case "i":
    case "o":
    case "u":
    case "A":
    case "E":
    case "I":
    case "O":
    case "U":
        type = "vowel"
        break
    default://any other letter is a consonant
    type = "consonant"
}
//This is desplayed on the html and shows the user's input and if it is a vowel or a consonant
console.log(`This application determines if your input is a vowel or a consonant:`)
console.log(`--------------------------------------------------------------------`)
console.log(`${letter} is a ${type}`)